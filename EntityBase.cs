using System.Collections.Generic;

namespace CallDynamicMethodInDynamicClass
{
    public class EntityBase<TEntity> where TEntity : IEntity
    {
        public IList<TEntity> Insert(TEntity entity)
        {
            var list = new List<TEntity>();

            list.Add(entity);

            list.Add(entity);

            list.Add(entity);

            return list;
        }
    }
}