﻿using System;
using Newtonsoft.Json;

namespace CallDynamicMethodInDynamicClass
{
    class Program
    {
        static void Main(string[] args)
        {

            var assembly = typeof(IEntity).Assembly;

            var findType = assembly.GetType("CallDynamicMethodInDynamicClass.Entity");
            var entityType = assembly.GetType("CallDynamicMethodInDynamicClass.Entity");

            var entityInstance = Activator.CreateInstance(entityType);

            var entityInsertMethod = entityType.GetMethod("Insert");

            var json = "{ \"name\": \"nima\" }";

            var data = JsonConvert.DeserializeObject<dynamic>(json);

            entityInsertMethod.Invoke(entityInstance, new object[] { entityInstance });

        }
    }
}
