using System.Collections.Generic;
using System.Reflection;

namespace CallDynamicMethodInDynamicClass
{
    public interface IInstanceService
    {
        IList<PropertyInfo> GetPropertyInfosFromInstance(object instance);

        PropertyInfo GetPropertyInfoFromInstance(object instance);
    }
}