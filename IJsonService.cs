using System.Collections.Generic;

namespace CallDynamicMethodInDynamicClass
{
    public interface IJsonService
    {
        IList<JsonKeyValue> GetJsonKeyValueProperties(string jsonData);

        dynamic DeserializeJsonToDynamicClass(string jsonData);

        JsonKeyValue GetKeyValueFromJsonProperty(/* hanooz nemidoonam bayad inja chi bezaram */);
    }
}