using System.Reflection;

namespace CallDynamicMethodInDynamicClass
{
    public interface IMapperService
    {
        // todo gharar shod data ha az json biyad toye instance dynamic ke sakhte shode
        // oonja doone doone property haro map konam too ham
        // baad az in ghesmat faghat kafiye ke instance bere be method khode instance
        // vaghean aali mishe in kar :D enghad zogh daram ke hamin emshab anjam bedam
        object MapFromJsonToInstance(string json, object instance);

        // first check property info lowercase name to json key
        // after found right key then map key value to property info value
        object MapJsonKeyValueToPropertyInfo(JsonKeyValue keyValue, PropertyInfo propertyInfo);
    }
}