namespace CallDynamicMethodInDynamicClass
{
    public class Entity : EntityBase<Entity>, IEntity
    {
        public Entity() {}
        
        public Entity(string id, string name)
        {
            Id = id;

            Name = name;
        }

        public string Id { get; set; }

        public string Name { get; set; }
    }
}