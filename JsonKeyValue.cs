namespace CallDynamicMethodInDynamicClass
{
    public class JsonKeyValue
    {
        /// <summary>
        /// lowercase property name should save in key property
        /// </summary>
        /// <value></value>
        public string Key { get; set; }

        public object Value { get; set; }

        public string PropertyName { get; set; }
    }
}